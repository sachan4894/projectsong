from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from ..models import User as UserModel
from ..forms import Signup as SignupForm
def Signup(GenericAPIView):
	def post(self, request, *args, **kwargs):
		form = SignupForm(request.POST, request.FILES)
		if form.is_valid():
			newEntry = UserModel(name = request.FILES['name'], email = request.FILES['email'],image = request.FILES['image'],mobile = request.FILES['mobile'])
			newEntry.save()

			return HttpResponseRedirect(reverse('songs.music.views.signup.Signup'))
		else:
			form = SignupForm()

		entries = UserModel.objects.all()
		return render_to_response('music/signup.html',{'documents': documents, 'form': form},context_instance=RequestContext(request))

def index(request):
	return render_to_response('music/index.html')				