from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST,\
    HTTP_404_NOT_FOUND

from ..models import User as UserModel
from ..serializers import User as UserSerializer

class User(GenericAPIView):
	def get_serializer_class(self):
		return UserSerializer
	
	def get(self, request, userid, *args, **kwargs):
		try:
			user = UserModel.objects.get(userid=userid)
		except UserModel.DoesNotExist:
			return Response(status=HTTP_404_NOT_FOUND)

		serializer = self.get_serializer_class()(data=user)
		return Response(serializer.data)

	def post(self, request, userid, *args, **kwargs):
		serializer = UserSerializer(data=request.data)
        
		if serializer.is_valid():
			serializer.save(userid=userid)
			return Response(status=HTTP_201_CREATED)
		
		return Response(status=HTTP_400_BAD_REQUEST)
        
	def put(self, request, userid, *args, **kwargs):
		try:
			user = UserModel.objects.get(userid=userid)
		except UserModel.DoesNotExist:
			return Response(status=HTTP_404_NOT_FOUND)
        
		serializer = UserSerializer(user, data=request.data)
		if not serializer.is_valid():
			return Response(status=HTTP_400_BAD_REQUEST)
        
		serializer.save(userid=userid)
		return Response(HTTP_201_CREATED)

class UserDetail(GenericAPIView):
	def get_serializer_class(self):
		return UserSerializer
	
	def get(self, request, userid, *args, **kwargs):
		user = UserModel.objects.all()
		serializer = self.get_serializer_class()(data=user, many=True)
		return Response(serializer.data)		    			    