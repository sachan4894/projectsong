from django.conf.urls import patterns, url, include
from views import User, Signup

urlpatterns = patterns('',
	url(r'(?P<userid>.+)/user/$', User.as_view()),
	url(r'signup/$', Signup.as_view())
	)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)