from django.db import models
from .base import Base
from uuid import uuid4

def generateGUID(): return uuid4().hex

class User(Base):
	userid = models.CharField(max_length=64, primary_key=True, default=generateGUID)
	name = models.CharField(max_length=128)
	email = models.EmailField(max_length=128)
	image = models.ImageField(upload_to='pic_upload/%Y/%m/%d', default='pic_upload/None/no-img.jpg')
	mobile = models.IntegerField()
