from rest_framework import serializers

from ..models import User as UserModel

class User(serializers.Serializer):
    name = serializers.CharField(max_length=128)
    email = serializers.EmailField(max_length=128)
    image = serializers.ImageField(default='pic_upload/None/no-img.jpg')
    mobile = serializers.IntegerField()

    def create(self, validated_data):
        return UserModel.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get('email', instance.email)
        instance.image = validated_data.get('image', instance.image)
        instance.mobile = validated_data.get('mobile', instance.mobile)
        instance.save()
        return instance