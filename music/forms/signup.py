from django import forms

class Signup(forms.Forms):
	name = forms.CharField()
	email = forms.EmailField()
	image = forms.ImageField()
	mobile = forms.IntegerField()
