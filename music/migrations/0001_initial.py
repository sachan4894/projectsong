# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import music.models.user


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('createdOn', models.DateTimeField(auto_now_add=True)),
                ('updatedOn', models.DateTimeField(auto_now=True)),
                ('userid', models.CharField(default=music.models.user.generateGUID, max_length=64, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=128)),
                ('image', models.ImageField(default=b'pic_upload/None/no-img.jpg', upload_to=b'pic_upload/%Y/%m/%d')),
                ('mobile', models.IntegerField(max_length=20)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
